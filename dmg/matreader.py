#213

import scipy.io
from glob import glob
import os

files_list = glob(os.path.join('mat', '*.mat'))
files_list_1 = []
files_list_2 = []
files_list_3 = []
for x in files_list:
	files_list_1.append(x[4:7])
	files_list_2.append(x[8:10])
	files_list_3.append(x[11:14])
sorted_list_1 = sorted(files_list_1)
sorted_list_2 = sorted(files_list_2)
sorted_list_3 = sorted(files_list_3)
final_list = []
for x in sorted_list_2:
	if glob(os.path.join('mat', '*'+x+'*')) not in final_list:
		final_list.append(glob(os.path.join('mat', '*'+x+'*')))
final_arr = []
i = 0
for x in final_list:
	for y in x:
		final_arr.append(scipy.io.loadmat(y))
		final_arr[i]['gesture'] = y[5:7]
		final_arr[i]['tester'] = y[8:10]
		final_arr[i]['trial'] = y[12:14]
		i = i+1
print final_arr[0]
